import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider } from "react-redux"
import store from "./redux/store"
import cartReducer, {getTotals} from "./redux/cartRedux";
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

store.dispatch((getTotals));


let persistor = persistStore(store);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <App />
            </PersistGate>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

