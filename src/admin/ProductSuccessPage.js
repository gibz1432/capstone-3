import {useEffect} from 'react'
import { useSelector} from'react-redux'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import {Container} from 'react-bootstrap'



export default function ProductSuccessPage(){
const navigate = useNavigate()

const handleNavigate = ()=>{
	navigate("/admin/products")
}



	return(

			
			<>
				<Container className="success-user mt-5">
					<div class="jumbotron text-center">
	  					<h1 class="display-3">Success!</h1>
	  					<p class="lead"><strong>Product successfully archived. Please check the updated list of products.</strong></p>
	  					<hr/>
	  					<p class="lead">
	    				<button className="btn btn-success btn-lg" role="button" onClick={handleNavigate}>OK</button>
	  					</p>
					</div>
				</Container>
			</>

		)

}