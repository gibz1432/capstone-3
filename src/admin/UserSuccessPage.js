import {useEffect} from 'react'
import { useSelector} from'react-redux'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import {Container} from 'react-bootstrap'



export default function UserProcessing(){
const userDetail = useSelector(state => state.auth.userDetail)
const JWT = localStorage.getItem('token')
const navigate = useNavigate()

const handleNavigate = ()=>{
	navigate("/admin/users")
}



	return(

			
			<>
				<Container className="success-user mt-5">
					<div class="jumbotron text-center">
	  					<h1 class="display-3">Success!</h1>
	  					<p class="lead"><strong>User successfully set to Admin. Please check the updated list of users.</strong></p>
	  					<hr/>
	  					<p class="lead">
	    				<button className="btn btn-success btn-lg" role="button" onClick={handleNavigate}>OK</button>
	  					</p>
					</div>
				</Container>
			</>

		)

}