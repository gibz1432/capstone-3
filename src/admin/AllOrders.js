import { Container } from 'react-bootstrap'
import { useEffect } from 'react'
import { useDispatch,useSelector } from 'react-redux'
import { getAllOrders,resetOrder } from "../redux/orderSlice"
import { useNavigate } from "react-router-dom"
import Swal from 'sweetalert2'
import OrdersCard from '../components/OrdersCard'
import {Link} from 'react-router-dom'

export default function AllOrders(){


const orders = useSelector(state => state.order.order)
const isError = useSelector(state => state.product.isError)
const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product.isLoading)
const message = useSelector(state => state.product.message)

const dispatch = useDispatch();


useEffect(() => {
	if(isError){
		console.log(message)
	}

	
	dispatch(getAllOrders())


	// return () => {
	// 	dispatch(resetOrder())
	// }
},[isError,message, dispatch])



// console.log(orders)


// **************************WHEELS******************************//
		const ordersList = orders.map(order => {
		return (

			<OrdersCard key = {order._id} orderProp = {order}/>
			)
	})

		// console.log(ordersList)






	
	return(
		<>
			<Container className="container">

			<div className="d-flex">
   				<div className="me-auto mb-2 mt-2"><h1>All Orders</h1></div>
   			<Link className="products-link" to="/admin/dashboard">	
   				<div className="d-flex mt-3">
		   			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mt-1 mx-1" viewBox="0 0 16 16">
		  			<path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
					</svg>
		   				<div className="mt-0">Return to Dashboard
		   				</div>
		   		</div>
   			</Link>	
   			</div>	

   				<div className="mt-3 d-flex justify-content-between main-font">
					<div className="order-product main-font"><h5>User</h5></div>
					<div className="order-product main-font"><h5>Product</h5></div>
					<div><h5>Price</h5></div>
					<div><h5>Quantity</h5></div>
					<div><h5>Total</h5></div>
				</div>
				<hr/>

		{
			orders.map(order => (
			
				<div key={order._id}>

				{order.products.map(product => (
				<div key={product.productId}>

					<div className="d-flex justify-content-between">
						<div className="flex-column align-self-center order-product-name main-font">
                    		<h6>{order.user.fullName}</h6>
          				</div>
            
                		<div className="flex-column align-self-center order-product-name main-font">
                    		<h6>{product.name}</h6>
          				</div>
              			<div className="d-flex" sm={12} md={3}>
                		<div className="flex-column align-self-center main-font">
                   	 		<span>&#8369;</span>{product.price}
                		</div>
              			</div>
              			<div className="d-flex" sm={12} md={3}>
                  		<div className="flex-column align-self-center main-font">
                    		{product.quantity}
                  		</div>
              			</div>

             			<div className="d-flex" sm={12} md={3}>   
                  		<div className="flex-column align-self-center main-font">   
                      		<span>&#8369;</span>{product.quantity*product.price}
                  		</div>     
              		</div>
        
      			</div>

				</div>	
						))}

				      <div>   				          
				            <div className="d-flex justify-content-between main-font">
				                <div><h5 className="text-danger">Order Total</h5></div>
				                <div><h5 className="text-danger">
				                <span>&#8369;</span>{order.totalAmount}</h5></div>
				            </div>
				            <div>   
				                Date Purchased: {order.purchasedOn}
				            </div>
				      </div>
				      <hr/>
				</div>

				))
		}
			

	



		



	

			
				 
			</Container>
		</>

		)

}