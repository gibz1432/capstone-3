import { useState, useEffect } from 'react'
// import { Link, useNavigate } from 'react-router-dom'
import { useDispatch,useSelector } from 'react-redux'
// import { toast } from 'react-toastify'
import { createProduct,reset } from "../redux/productSlice"
import Swal from 'sweetalert2'
import {Link} from 'react-router-dom'
// import Spinner from '../components/Spinner'





export default function Products(){
const products = useSelector(state => state.product.products)
const isError = useSelector(state => state.product.isError)
const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product.isLoading)
const message = useSelector(state => state.product.message)


	const [ formData, setFormData ] = useState({
		name: '',
		categoryId: '',
		description: '',
		img: '',
		tags: '',
		size:'',
		color:'',
		price:'',
		numberOfStocks:''
	})

const { name, categoryId, description, img, tags, size, color, price, numberOfStocks } = formData
const dispatch = useDispatch();

useEffect(() => {
	// if(isError){
	// 	alert(message)
	// }

	if(isSuccess){
	alert('Product successfully created')
	}

	return () => {
		dispatch(reset())
	}
},[isError,message, dispatch])

	// const navigate = useNavigate();
	

 const onChange = (e) => {
 	setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()
          const userData = {
        name,
        categoryId,
        description,
        img,
        tags,
        size,
        color,
        price,
        numberOfStocks
      }

      dispatch(createProduct(userData))
    
  }

return(
 			<>		
<section className="add-product-background" >
  <div className="mask d-flex align-items-center h-100 gradient-custom-3">
    <div className="container h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
          <div className="card">
            <div className="card-body p-5">
              <h2 className="text-uppercase text-center mb-5">Products</h2>

              <form onSubmit={onSubmit}>

                <div className="form-group form-outline mb-4">
                  <input type="text" name="name" id="name" className="form-control form-control-lg" value={name} placeholder="Product Name" required onChange={onChange}/>
                </div>

                <div className="form-group form-outline mb-4">
                  <input type="text" name="categoryId" id="categoryId" className="form-control form-control-lg" value={categoryId} placeholder="Category Id" required onChange={onChange}/>
                </div>


                <div className="form-group form-outline mb-4">
                  <input type="text" name="description" id="description" className="form-control form-control-lg" value={description} placeholder="Description" required onChange={onChange}/>
                </div>

                <div className="form-group form-outline mb-4">
                  <input type="text" name="img" id="img" className="form-control form-control-lg" value={img} placeholder="Image URL" required onChange={onChange}/>
                  
                </div>

                <div className="form-group form-outline mb-4">
                  <input type="text" name="tags" id="tags" className="form-control form-control-lg" value={tags} placeholder="Search tag" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
                  <input type="text" name="size" id="size" className="form-control form-control-lg" value={size} placeholder="Size" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
                  <input type="text" name="color" id="color" className="form-control form-control-lg" value={color} placeholder="Color" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
                  <input type="text" name="price" id="price" className="form-control form-control-lg" value={price} placeholder="Price" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
                  <input type="text" name="numberOfStocks" id="numberOfStocks" className="form-control form-control-lg" value={numberOfStocks} placeholder="Number of Stocks" required onChange={onChange}/>
                </div>

 

                <div className="form-group d-flex justify-content-center">
                  <button type="submit" className="register-button">Add Product</button>
                </div>

                          <Link className="products-link" to="/admin/dashboard">  
          <div className="d-flex mt-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mt-1 mx-1" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
          </svg>
              <div className="mt-0">Return to Dashboard
              </div>
          </div>
        </Link> 

        

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>




</section>
		</>

 	)

}



