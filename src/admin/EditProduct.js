import { useState, useEffect } from 'react'
// import { Link, useNavigate } from 'react-router-dom'
import { useDispatch,useSelector } from 'react-redux'
// import { toast } from 'react-toastify'
import { createProduct,reset,updateProduct } from "../redux/productSlice"
import Swal from 'sweetalert2'
import {Link, useParams} from 'react-router-dom'
import axios from 'axios'
// import Spinner from '../components/Spinner'





export default function EditProduct(){
const products = useSelector(state => state.product.products)
const isError = useSelector(state => state.product.isError)
const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product.isLoading)
const message = useSelector(state => state.product.message)
const JWT = localStorage.getItem('token')

 function refreshPage() {
    window.location.reload(false);
  }

	const [ formData, setFormData ] = useState({
		name: '',
		description: '',
		img: '',
		tags: '',
		size:'',
		color:'',
		price:'',
		numberOfStocks:''
	})

const { name, description, img, tags, size, color, price, numberOfStocks } = formData
const dispatch = useDispatch();
 const { id } = useParams();



useEffect(() => {

	if(isError){
		console.log(message)
	}

	const getProduct = async() =>{
		try{
			const res = await axios.get("https://bikada-bikeshop.onrender.com/products/" + id)
				setFormData(res.data);


		}catch (err){
			console.log(err)
		}
		
	}
	getProduct();


},[id, isError])

	

 const onChange = (e) => {
 	setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = async(e) => {
    e.preventDefault()
          const productData = {
        name,
        description,
        img,
        tags,
        size,
        color,
        price,
        numberOfStocks
      }

      let token = JSON.parse(JWT)
axios.put("https://bikada-bikeshop.onrender.com/products/" + id ,productData, { headers: {"Authorization" : `Bearer ${token}`} })
.then(res => 	Swal.fire({
					title: `Success`,
					icon: "success",
					text: `Product successfuly updated`
				}))
.catch(error => alert(error));
// editProduct(productData); // dispatch(updateProduct(id,productData))
    
  }

return(
 			<>		
<section className="edit-product-background" >
  <div className="mask d-flex align-items-center h-100 gradient-custom-3">
    <div className="container h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
          <div className="card">
            <div className="card-body p-5">
              <h2 className="text-uppercase text-center mb-5">Products</h2>

              <form onSubmit={onSubmit}>

                <div className="form-group form-outline mb-4">
                	Product Name
                  <input type="text" name="name" id="name" className="form-control form-control-lg" value={name || ''} placeholder="Product Name" required onChange={onChange}/>
                </div>
                	
                <div className="form-group form-outline mb-4">
                	Description:
                  <textarea type="text" name="description" id="description" className="form-control form-control-lg" value={description || ''} placeholder="Description" required onChange={onChange}></textarea>
                </div>

                <div className="form-group form-outline mb-4">
                	Image Url
                  <input type="text" name="img" id="img" className="form-control form-control-lg" value={img || ''} placeholder="Image URL" required onChange={onChange}/>
                  
                </div>

                <div className="form-group form-outline mb-4">
                	Search Tags
                  <input type="text" name="tags" id="tags" className="form-control form-control-lg" value={tags || ''} placeholder="Search tag" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
            		Size
                  <input type="text" name="size" id="size" className="form-control form-control-lg" value={size || ''} placeholder="Size" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
            		Color
                  <input type="text" name="color" id="color" className="form-control form-control-lg" value={color || ''} placeholder="Color" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
            		Price
                  <input type="text" name="price" id="price" className="form-control form-control-lg" value={price || ''} placeholder="Price" required onChange={onChange}/>
                </div>


            <div className="form-group form-outline mb-4">
            		Number of stocks
                  <input type="text" name="numberOfStocks" id="numberOfStocks" className="form-control form-control-lg" value={numberOfStocks || ''} placeholder="Number of Stocks" required onChange={onChange}/>
                </div>

 

                <div className="form-group d-flex justify-content-center">
                  <button type="submit" className="register-button">Update Product</button>
                </div>

                          <Link className="products-link" to="/admin/dashboard">  
          <div className="d-flex mt-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mt-1 mx-1" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
          </svg>
              <div className="mt-0">Return to Dashboard
              </div>
          </div>
        </Link> 

              </form>

            </div>
          </div>
        </div>
      </div>

    </div>


  </div>





</section>
		</>

 	)

}



