import { useEffect,useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getAllUsers,reset, setAdmin, setToAdmin} from "../redux/authSlice"
import Spinner from '../components/Spinner'
import Swal from 'sweetalert2'



export default function Users(){


const dispatch = useDispatch()
const navigate = useNavigate()
const users = useSelector(state => state.auth.user)
// const isAdmin = useSelector(state => state.auth.isAdmin)
const isError = useSelector(state => state.auth.isError)
const isSuccess = useSelector(state => state.auth.isSuccess)
const isLoading = useSelector(state => state.auth.isLoading)
const message = useSelector(state => state.auth.message)

useEffect(() => {



	if(isError){
		alert(message)
	}



	dispatch(getAllUsers())


	return () => {
		dispatch(reset())
	}



},[isError,message, dispatch,navigate])


useEffect(() => {
dispatch(getAllUsers())
},[dispatch])



// *******************setAdmin*************************//
	
	const handleSetAdmin = (user) =>{
	
		let userId = user._id
		dispatch(setAdmin({userId}));
			// if(isSuccess){
			// 				Swal.fire({
			// 		title: `Success`,
			// 		icon: "success",
			// 		text: `User successfuly set to Admin`
			// 	})
				navigate("/admin/success/user")
	// }
		


	}

	        const tableBody = Object.values(users).map(user => {

	   return(   	
          <tbody key={user._id}>
            <tr>
                <th scope="row">{user.fullName}</th>
                <td>{user.email}</td>
                <td>{user.mobileNo}</td>
                <td>{user.isAdmin? "true": "false"}</td>
                <td>{!user?.isAdmin && <button className="btn btn-primary" onClick={(e) => handleSetAdmin(user)}>Set as Admin</button>} 
                </td>
            </tr>
          </tbody>
          )
          })



	if(isLoading){
		return <Spinner/>
	}

  return(
    <>  
			<div className="d-flex">
   				<div className="me-auto mb-2 mt-2"><h1>USERS</h1></div>
   			<Link className="products-link" to="/admin/dashboard">	
   				<div className="d-flex mt-3">
		   			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mt-1 mx-1" viewBox="0 0 16 16">
		  			<path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
					</svg>
		   				<div className="mt-0">Return to Dashboard
		   				</div>
		   		</div>
   			</Link>	
   			</div>	
      <table className="table table-hover">
          <thead>
            <tr className="bg-primary">
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Mobile Number</th>
                <th scope="col">isAdmin</th>
                <th scope="col">Actions</th>
            </tr>
          </thead>
          	{tableBody}
      </table>

    </>

    )
}