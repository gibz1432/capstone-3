import { useEffect,useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getAllProducts,reset, disableProduct} from "../redux/productSlice"
import Spinner from '../components/Spinner'
import Swal from 'sweetalert2'


export default function Products(){
const navigate = useNavigate()
const[addFormData, setAddFormData] = useState({
    name: '',
    categoryId: '',
    description: '',
    img: '',
    tags: '',
    size:'',
    color:'',
    price:'',
    numberOfStocks:''
})




const handleAddFormChange = (e) => {
  e.preventDefault();

    const fieldName = e.target.getAttribute('name');
    const fieldValue = e.target.value;

    const newFormData = {...addFormData};
    newFormData[fieldName] = fieldValue;

    setAddFormData(newFormData);
}

const handleAddFormSubmit = (e) => {
  e.preventDefault();

  const newProduct = {
        name: addFormData.fullName,
        categoryId: addFormData.categoryId,
        description: addFormData.description,
        img: addFormData.img,
        tags: addFormData.tags,
        size: addFormData.size,
        color: addFormData.color,
        price: addFormData.price,
        numberOfStocks: addFormData.numberOfStocks

  }
}


const dispatch = useDispatch()
const products = useSelector(state => state.product.products)
// const isAdmin = useSelector(state => state.auth.isAdmin)
const isError = useSelector(state => state.product.isError)
const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product.isLoading)
const message = useSelector(state => state.product.message)

useEffect(() => {


  if(isError){
    alert(message)
  }

  dispatch(getAllProducts())

  return () => {
    dispatch(reset())
  }
},[isError,message, dispatch])


// *******************Disable Product*************************//
  
  const handleDisableProduct = (product) =>{
  
    let productId = product._id
    dispatch(disableProduct({productId}));
      // if(isSuccess){
      //        Swal.fire({
      //    title: `Success`,
      //    icon: "success",
      //    text: `User successfuly set to Admin`
      //  })
        navigate("/admin/success/product")

  }



            const tableBody = Object.values(products).map(product => {

     return(    
          <tbody key={product._id}>
            <tr>
                <th scope="row">{product.name}</th>
                {/*<td>{product.category.categoryId} {product.category.categoryName}</td>*/}
                <td>{product.description}</td>
                {/*<td className="table-img">{product.img}</td>*/}
                <td>{product.size}</td>
                <td>{product.color}</td>
                <td>{product.price}</td>
                <td>{product.numberOfStocks}</td>
                <td>{product.isAvailable? "true": "false"}</td>
                <td>
                <Link to={`/admin/products/update/${product._id}`}>
                  <button className="btn btn-primary">Edit
                  </button>
                </Link>
                    {product.isAvailable && <button className="btn btn-danger" onClick={() => handleDisableProduct(product)}>Disable</button>} 
                </td>
            </tr>
          </tbody>

          )
          })






  if(isLoading){
    return <Spinner/>
  }

  return(
    <>  
        <div className="d-flex">
          <div className="me-auto mb-2 mt-2"><h1>PRODUCTS</h1></div>

        <Link className="products-link" to="/admin/dashboard">  
          <div className="d-flex mt-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mt-1 mx-1" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
          </svg>
              <div className="mt-0">Return to Dashboard
              </div>
          </div>
        </Link> 
        </div>  

        <Link to="/admin/products/add" className="products-link">
         <div className="d-flex justify-content-center mb-2"><button className="btn btn-success">Add Product</button></div>
        </Link> 
   <form>
      <table className="table table-hover">
          <thead>
            <tr className="bg-primary">
                <th scope="col">Name</th>
                {/*<th scope="col">Category</th>*/}
                <th scope="col">Description</th>
                {/*<th scope="col">Image Url</th>*/}
                <th scope="col">Size</th>
                <th scope="col">Color</th>
                <th scope="col">Price</th>
                <th scope="col">Number of Stocks</th>
                <th scope="col">isAvailable</th>
                <th scope="col">Actions</th>
            </tr>
          </thead>
          {tableBody}
      </table>
  </form>
    </>

    )
}