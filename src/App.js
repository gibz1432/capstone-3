import Header from './components/Header'
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import { BrowserRouter as Router} from 'react-router-dom'
import { Routes, Route} from 'react-router-dom'
import { Container } from "react-bootstrap";


import Home from './pages/Home'
import Bikes from './pages/Bikes'
import Frames from './pages/Frames'
import Helmets from './pages/Helmets'
import Tires from './pages/Tires'
import Wheels from './pages/Wheels'
import Cart from './pages/Cart'
import Register from './pages/Register'
import Login from './pages/Login'
import ProductView from './pages/ProductView'
import Products from './admin/Products'
import Users from './admin/Users'
import UserSuccessPage from './admin/UserSuccessPage'
import ProductSuccessPage from './admin/ProductSuccessPage'
import AllOrders from './admin/AllOrders'
import AddProducts from './admin/AddProducts'
import EditProduct from './admin/EditProduct'
import Orders from './pages/Orders'
import AdminDashBoard from './pages/AdminDashBoard'
import PageNotFound from './pages/PageNotFound'
import { ToastContainer } from "react-toastify"
import { useSelector} from'react-redux'

import './App.css';
import "react-toastify/dist/ReactToastify.css"


function App() {

const userDetail = useSelector(state => state.auth.userDetail)


  return (
    <>
      <Router>
        <ToastContainer/>
        <Header/>
        {(!userDetail || !userDetail || !userDetail.isAdmin) && <Navbar/>}
        <Container className="page-container">
          <div className="content-wrap">
           <Routes>
            <Route exact path= "/bikes" element={<Bikes/>}/>
            <Route exact path= "/frames" element={<Frames/>}/>
            <Route exact path= "/helmets" element={<Helmets/>}/>
            <Route exact path= "/tires" element={<Tires/>}/>
            <Route exact path= "/wheels" element={<Wheels/>}/>
            <Route exact path= "/register" element={<Register/>}/>
            <Route exact path= "/login" element={<Login/>}/>
            <Route exact path= "/cart" element={<Cart/>}/>
            <Route exact path= "/orders" element={<Orders/>}/>
            <Route exact path= "/admin/products" element={<Products/>}/>
            <Route exact path= "/admin/users" element={<Users/>}/>
            <Route exact path= "/admin/orders" element={<AllOrders/>}/>
            <Route exact path= "/admin/products/add" element={<AddProducts/>}/>
            <Route exact path= "/admin/products/update/:id" element={<EditProduct/>}/>
            <Route exact path= "/" element={<Home/>}/>
            <Route exact path= "/products/:id" element={<ProductView/>}/>
            <Route exact path= "/admin/dashboard" element={<AdminDashBoard/>}/>
            <Route exact path= "/admin/success/user" element={<UserSuccessPage/>}/>
            <Route exact path= "/admin/success/product" element={<ProductSuccessPage/>}/>
            <Route exact path= "*" element={<PageNotFound/>}/>
          </Routes>
         </div>
        </Container>
        <Footer/> 
      </Router>
    </>  
  );
}

export default App;
