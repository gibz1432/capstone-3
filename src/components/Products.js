import {Col, Row} from 'react-bootstrap'
import { useEffect } from 'react'
import { Container } from "react-bootstrap";
import ProductCard from '../components/ProductCard'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getBikes,getFrames,getHelmets,getTires,getWheels, reset} from "../redux/productSlice"
import Spinner from '../components/Spinner'



export default function Products(){
const dispatch = useDispatch()

const bikes = useSelector(state => state.product?.bikes)
const frames = useSelector(state => state.product?.frames)
const helmets = useSelector(state => state.product?.helmets)
const tires = useSelector(state => state.product?.tires)
const wheels = useSelector(state => state.product?.wheels)
const isError = useSelector(state => state.product?.isError)
// const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product?.isLoading)
const message = useSelector(state => state.product?.message)

useEffect(() => {
	if(isError){
		console.log(message)
	}

	dispatch(getBikes())
	dispatch(getFrames())
	dispatch(getHelmets())
	dispatch(getTires())
	dispatch(getWheels())

	return () => {
		dispatch(reset())
	}
},[isError,message, dispatch])


// **************************BIKES******************************//
	const bikeslist = bikes?.slice(0,4)?.map(bike => {
		return (

			<ProductCard key = {bike._id} productProp = {bike}/>
			)
	})


// **************************FRAMES******************************//
	const framesList = frames?.slice(0,4)?.map(frames => {
		return (

			<ProductCard key = {frames._id} productProp = {frames}/>
			)
	})


// **************************HELMETS******************************//
	const helmetsList = helmets?.slice(0,4)?.map(helmets => {
		return (

			<ProductCard key = {helmets._id} productProp = {helmets}/>
			)
	})


// **************************TIRES******************************//
	const tiresList = tires?.slice(0,4)?.map(tires => {
		return (

			<ProductCard key = {tires._id} productProp = {tires}/>
			)
	})


// **************************WHEELS******************************//
	const wheelsList = wheels?.slice(0,4)?.map(wheels => {
		return (

			<ProductCard key = {wheels._id} productProp = {wheels}/>
			)
	})






	if(isLoading){
		return <Spinner/>
	}

	return(
	<>	

		<hr/>
		<Container className="bikes">
			<div className="d-flex">
   				<div className="me-auto mb-2"><h1>BIKES</h1></div>
   			<Link className="products-link" to="/bikes">	
   				<div className="mt-3">More bikes
   				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
  				<path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
				</svg>
   				</div>
   			</Link>	
   			</div>	
   			<Row className="row">
				<Col className="d-flex my-4" sm={6} md={3}>
					{bikeslist[0]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{bikeslist[1]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{bikeslist[2]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{bikeslist[3]}
				</Col>
			</Row>	
		</Container>


		<hr/>
		<Container className="frames">
			<div className="d-flex">
   				<div className="me-auto mt-5 pb-0"><h1>FRAMES</h1></div>
   			<Link className="products-link" to="/frames">	
   				<div className="mt-5 pb-0">More frames
   				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
  				<path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
				</svg>
   				</div>
   			</Link>	
   			</div>	
   			
   			<Row className="row">
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[0]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[1]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[2]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[3]}
				</Col>
			</Row>
		</Container>


		<hr/>
		<Container className="helmets">
			<div className="d-flex">
   				<div className="me-auto mt-5 pb-0"><h1>HELMETS</h1></div>
   			<Link className="products-link" to="/helmets">	
   				<div className="mt-5 pb-0">More helmets
   				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
  				<path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
				</svg>
   				</div>
   			</Link>	
   			</div>	
   			
   			<Row className="row">
				<Col className="d-flex my-4" sm={6} md={3}>
					{helmetsList[0]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{helmetsList[1]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{helmetsList[2]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{helmetsList[3]}
				</Col>
			</Row>
		</Container>

		<hr/>
		<Container className="tires mb-5">
			<div className="d-flex">
   				<div className="me-auto mt-5 pb-0"><h1>TIRES</h1></div>
   			<Link className="products-link" to="/tires">	
   				<div className="mt-5 pb-0">More tires
   				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
  				<path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
				</svg>
   				</div>
   			</Link>	
   			</div>	
   			
   			<Row className="row mb-5 pb-5">
				<Col className="d-flex my-4" sm={6} md={3}>
					{tiresList[0]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{tiresList[1]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{tiresList[2]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{tiresList[3]}
				</Col>
			</Row>
		</Container>
		<div className="mt-5">
			<hr className="mt-5"/>
		</div>
		<Container className="wheels--container mb-5">
			<div className="d-flex mt-5">
   				<div className="me-auto pb-0"><h1>WHEELS</h1></div>
   			<Link className="products-link" to="/wheels">	
   				<div className="pb-0">More wheels
   				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
  				<path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
				</svg>
   			</div>
   			</Link>	
   			</div>	
   			
   			<Row className="row">
				<Col className="d-flex my-4" sm={6} md={3}>
					{wheelsList?.[0]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{wheelsList?.[1]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{wheelsList?.[2]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{wheelsList?.[3]}
				</Col>
			</Row>
		</Container>



	</>

		)
}