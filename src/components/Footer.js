import "./Footer.css";

export default function Footer() {
  return (
    <div className="main-footer">
      <div className="container">
        <div className="row">
          {/* Column1 */}
          <div className="col">
            <h5>BiKada Bicycle Shop</h5>
            <h1 className="footer-text">
              <li>225-9625</li>
              <li>Lahug, Cebu City</li>
              <li>The North Park</li>
            </h1>
          </div>
          {/* Column2 */}
          <div className="col">
            <h5>Support</h5>
            <ui className="footer-text">
              <li>Owner's Manuals</li>
              <li>Bike Registration</li>
              <li>Terms and Conditions</li>
            </ui>
          </div>
          {/* Column3 */}
          <div className="col">
            <h5>Brand</h5>
            <ui className="footer-text">
              <li>Giant Bicycles</li>
            </ui>
          </div>
        </div>
        <hr />
        <div className="row">
          <p className="col-sm">
            &copy;{new Date().getFullYear()} BiKada Bicycle Shop | All rights reserved |
            Terms Of Service | Privacy
          </p>
        </div>
      </div>
    </div>
  );
}

