import Carousel from 'react-bootstrap/Carousel'
import {Container} from 'react-bootstrap'


export default function CarouselHome(){
	return(

	<Container className="mt-5 mb-5">
		<Carousel>
		  <Carousel.Item interval={1000}>
		    <img
		      className="d-block w-100"
		      src="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646659828/BikeShop%20Products/carousel-image-1_mkc3dz.jpg"
		      alt="First slide"
		    />
{/*		    <Carousel.Caption>
		      <h3>First slide label</h3>
		      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		    </Carousel.Caption>*/}
		  </Carousel.Item>
		  <Carousel.Item interval={500}>
		    <img
		      className="d-block w-100"
		      src="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646659911/BikeShop%20Products/carousel-image-2_x5pbbi.jpg"
		      alt="Second slide"
		    />
{/*		    <Carousel.Caption>
		      <h3>Second slide label</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </Carousel.Caption>*/}
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646659925/BikeShop%20Products/carousel-image-3_w4htzo.jpg"
		      alt="Third slide"
		    />
{/*		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>*/}
		  </Carousel.Item>

		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646659936/BikeShop%20Products/carousel-image-4_c6qmef.jpg"
		      alt="Fourth slide"
		    />
{/*		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>*/}
		  </Carousel.Item>

		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646659946/BikeShop%20Products/carousel-image-5_u4133c.jpg"
		      alt="Fifth slide"
		    />
{/*		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>*/}
		  </Carousel.Item>
		</Carousel>
	</Container>
		)
}
