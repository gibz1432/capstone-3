import { useEffect, useState } from 'react'
import {Container,Col,Row,FormControl,Button, Badge,Form} from 'react-bootstrap'
import { FaShoppingCart } from "react-icons/fa"
import { FaSearch } from "react-icons/fa"
import { useSelector, useDispatch } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import {logout, reset} from "../redux/authSlice"
// import {getCart, resetCart} from "../redux/cartRedux"
import axios from 'axios'
import { toast } from 'react-toastify'
import CartBadge from "../components/CartBadge"

export default function Header() {

const navigate = useNavigate()
const dispatch = useDispatch()
let token = useSelector(state => state.auth.token)
const cart = useSelector(state => state.cart.cartItems);
const quantity = useSelector(state => state.cart.cartQuantity);
// const quantity = useSelector(state => state.cart.cartQuantity);
const [cartItems, setCartItems] = useState({});
const userDetail= JSON.parse(localStorage?.getItem('userDetail'))




const onLogout = () => {
	console.log('clicked')
	dispatch(logout())
	dispatch(reset())
	toast.success(`Logout Successful`, {
					position: "top-right"
				});
	navigate("/")
}

	return(
   <> 
<header className="banner">   
   	<Container>
   		<Row className="d-flex">
   			<Col className="d-flex px-0 justify-content-center justify-content-sm-center justify-content-md-start mt-3" sm={12} md={6}>
   			<Link to="/">
   				<img src="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646660124/BikeShop%20Products/logo_go1efe.png" alt="logo"/>
   			</Link>	
   			</Col>
   			
   			<Col sm={12} md={6} className="d-flex justify-content-center justify-content-sm-center justify-content-md-end">
   				<div className="d-flex flex-column mt-4 mb-3">
		      			<div className="d-flex justify-content-center justify-content-sm-center justify-content-md-end mb-2">
		      			<Link className="header-link" to="/login">
			      		    <div className="header-font"><b>{token? `Logged in as ${userDetail?.fullName}`: 'Sign in'}</b></div>
			      		</Link>
					        <div className="header-font px-1"><b>|</b></div>

					        {token?
					        	(
					        <div className="header-font"><button className="header-logout-register-button" onClick={onLogout}><b>{token? 'Log out': 'Create an Account'}</b></button></div>
					      		)
					        	:		
					        	(					      
					        <Link className="header-link" to={`/register`}>
					        	<div className="header-font"><b>Create an Account</b></div>
					     		 </Link>
					      	)
					        }
					    </div>

	{(!userDetail || !userDetail || !userDetail.isAdmin) && (
				<div className="d-flex header-position flex-row">
				      <Form className="d-flex search-box">
								    <FormControl className="search-box bg-transparent"
									      placeholder="Search all products..."
									      aria-label="Search all products..."
									      aria-describedby="basic-addon2"
									    />
									    <Button variant="transparent" id="button-addon2" className="search-button">
									      <FaSearch color='white'/>
									    </Button> 
				      </Form>

				      <Link to="/cart">
				        <Button variant="outline-success" className="cart-button"><FaShoppingCart color=' white' size='1.4rem'/> <span className="px-1">CART</span></Button>
				      </Link>
				      		
				       { (token && userDetail && (userDetail?._id) && quantity !== 0) && 
				       	<Badge bg="danger" className="cart-badge my-1 py-1.5">{quantity}</Badge>}
				       	{/*<div>{cartBadge}</div>*/}
				</div>

	)}			
				</div>  
   			</Col>
   		


   		</Row>
	</Container>	
</header>

  </>

		)

}





