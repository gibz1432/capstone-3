import { Card, Container } from "react-bootstrap";
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

export default function BikesCard({productProp}) {
// console.log(productProp.name)
// const {id, name, img, price} = productProp
const products = useSelector(state => state.product.products)

const {_id, name, img, price } = productProp



  return (
   <>

    <Link  className="text-link" to={`/products/${_id}`}>
      <Card className="products--bikes mb-5 border-0">
        <Card.Img variant="top" src={img} className="product-cards p-2" />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text >
          <span className="main-font"><b>&#8369;</b></span><span className="main-font"><b>{price.toLocaleString()}</b></span>
          </Card.Text>
        </Card.Body>
      </Card>
    </Link>

   </>
  );
}