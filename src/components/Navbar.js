import {Container,Col,Row,InputGroup,FormControl,Button, Navbar,Nav, NavDropdown,Form} from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default	function() {
	return(
		<>
<Navbar expand="lg" className="navbar navbar-dark">
  <Container>
    {/*<Navbar.Brand href="#">Navbar scroll</Navbar.Brand>*/}
    <Navbar.Toggle aria-controls="navbarScroll" className="burger-icon"/>
   
    <Navbar.Collapse id="navbarScroll">
      <Nav
        className="me-auto my-2 my-lg-0 nav-fill w-100"
        navbarScroll
      >
      	
	        <Nav.Link as={ Link } to="/" href="#action1" className="nav-list">HOME</Nav.Link>
	        <Nav.Link as={ Link } to="/bikes" href="#action2" className="nav-list">BIKES</Nav.Link>
	        <Nav.Link as={ Link } to="/frames" href="#action2" className="nav-list">FRAMES</Nav.Link>
	        <Nav.Link as={ Link } to="/helmets" href="#action2" className="nav-list">HELMETS</Nav.Link>
	        <Nav.Link as={ Link } to="/wheels" href="#action2" className="nav-list">WHEELS</Nav.Link>
	        <Nav.Link as={ Link } to="/tires" href="#" className="nav-list">TIRES</Nav.Link>
	    
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>

</>

		)

}