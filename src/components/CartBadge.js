import { Badge } from 'react-bootstrap'
import { useSelector } from 'react-redux'

export default function CartBadge({cartProps}){

let token = useSelector(state => state.auth.token)
	const {cartQuantity} = cartProps
	

	return(
		<>

			{ token && cartQuantity !== 0 && 
				       	<Badge bg="danger" className="cart-badge my-1 py-1.5">{cartQuantity}</Badge>}

		</>
		)
}