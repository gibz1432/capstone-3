import { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { toast } from 'react-toastify'
import { login, reset,getUserDetail} from "../redux/authSlice"
// import Swal from 'sweetalert2'
import Spinner from '../components/Spinner'


export default function Login(){

	const [ formData, setFormData ] = useState({
		email: '',
		password: ''
	})

  const navigate = useNavigate()
  const dispatch = useDispatch()

	

	const userDetail = useSelector(state => state.auth.userDetail)
	const token = useSelector(state => state.auth.token)
	const isError = useSelector(state => state.auth.isError)
	const isSuccess = useSelector(state => state.auth.isSuccess)
	const isLoading = useSelector(state => state.auth.isLoading)
	const message = useSelector(state => state.auth.message)
	const { email, password } = formData
	const JWT = localStorage.getItem('token')
	const userInfo = localStorage.getItem('userDetail')
   console.log(userInfo) 


	useEffect(() => {

    if (isError) {
      toast.error(message)
   		console.log(message)
    }

	if (JWT && userInfo) {
		toast.success(`Login Successful`, {
					position: "top-right"
				});


	 	navigate("/admin/dashboard")
	} 
 	
    dispatch(reset())
	

  }, [JWT,userInfo,isSuccess, isError, message, navigate, dispatch])

	


const fetchUserData = async(loginUser, data) =>{
	
	const log = await dispatch(loginUser(data))
	if(log)
		// return console.log(log.payload)
	return dispatch(getUserDetail(log.payload))
	
} 


  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()

    const userData = {
      email,
      password,
    }

	fetchUserData(login, userData)
    

  }


  if (isLoading) {
    return <Spinner />
  }


	return(
		<>
<section className="vh-100 bg-image" >
  <div className="mask d-flex align-items-center h-100 gradient-custom-3">
    <div className="container h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
          <div className="card">
            <div className="card-body p-5">
              <h2 className="text-uppercase text-center mb-5">Login Here</h2>

              <form onSubmit={onSubmit}>

                <div className="form-outline mb-4">
                  <input type="email" name="email" id="email" className="form-control form-control-lg" value={email} placeholder="Email" required onChange={onChange}/>
                </div>

                <div className="form-outline mb-4">
                  <input type="password" name="password" id="password" className="form-control form-control-lg" value={password} placeholder="Password" required onChange={onChange}/>
                  
                </div>

                <div className="d-flex justify-content-center">
                  <button type="submit" className="register-button" onClick={onSubmit}>Log in</button>
                </div>

                 <p className="text-center text-muted mt-5 mb-0">Don't have an account yet? <Link to="/register"><u>Register here</u></Link></p>


              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
		</>

		)
}