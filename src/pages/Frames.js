import {Col, Row} from 'react-bootstrap'
import { useEffect } from 'react'
import { Container } from "react-bootstrap";
import ProductCard from '../components/ProductCard'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getFrames, reset} from "../redux/productSlice"
import Spinner from '../components/Spinner'



export default function Frames(){
const dispatch = useDispatch()

const frames = useSelector(state => state.product.frames)

const isError = useSelector(state => state.product.isError)
// const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product.isLoading)
const message = useSelector(state => state.product.message)

useEffect(() => {
	if(isError){
		console.log(message)
	}

	dispatch(getFrames())

	return () => {
		dispatch(reset())
	}
},[isError,message, dispatch])


// **************************BIKES******************************//
	const framesList = frames.map(frame => {
		return (

			<ProductCard key = {frame._id} productProp = {frame}/>
			)
	})


	if(isLoading){
		return <Spinner/>
	}

	return(
	<>	

		<hr/>
		<Container className="bikes">
			<div className="d-flex">
   				<div className="me-auto mb-2"><h1>FRAMES</h1></div>
   			<Link className="products-link" to="/">	
   				<div className="d-flex">
		   			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mt-1 mx-1" viewBox="0 0 16 16">
		  			<path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
					</svg>
		   				<div className="mt-0">Back to Homepage
		   				</div>
		   		</div>
   			</Link>	
   			</div>	
   			<Row className="row">
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[0]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[1]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[2]}
				</Col>
				<Col className="d-flex my-4" sm={6} md={3}>
					{framesList[3]}
				</Col>
			</Row>	
		</Container>

	</>

		)
}