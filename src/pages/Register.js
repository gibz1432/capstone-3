import { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { toast } from 'react-toastify'
import { register, reset } from "../redux/authSlice"
import Swal from 'sweetalert2'
import Spinner from '../components/Spinner'




export default function Register(){

const user = useSelector(state => state.auth.user)
const isError = useSelector(state => state.auth.isError)
const isSuccess = useSelector(state => state.auth.isSuccess)
const isLoading = useSelector(state => state.auth.isLoading)
const message = useSelector(state => state.auth.message)


	const [ formData, setFormData ] = useState({
		fullName: '',
		email: '',
		mobileNo: '',
		password: '',
		confirmPassword: ''
	})

	const { fullName, email, mobileNo, password, confirmPassword } = formData
	const navigate = useNavigate();
	const dispatch = useDispatch();

useEffect(() => {

	
     if (isError) {
     	console.log(message)
      
				Swal.fire({
					title: `Invalid Input`,
					icon: "error",
					text: `${message}`
				})
   		
    }

    if (isSuccess && user) {
    	console.log(user)
    		Swal.fire({
			title: `Registration Successful`,
			icon: "success",
			text: `You can now login to start shopping.`
				})
      navigate("/login")
    }
    
    dispatch(reset())
  }, [user, isError, isSuccess, message, navigate, dispatch])

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()

    if (password !== confirmPassword) {
      toast.error('Passwords do not match')
    } else {
      const userData = {
        fullName,
        email,
        mobileNo,
        password,
        confirmPassword
      }

      dispatch(register(userData))
    }
  }

  if (isLoading) {
    return <Spinner />
  }
	// console.log(user)

	return(
		<>		
<section className="vh-100 bg-image*" >
  <div className="mask d-flex align-items-center h-100 gradient-custom-3">
    <div className="container h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
          <div className="card">
            <div className="card-body p-5">
              <h2 className="text-uppercase text-center mb-5">Create an account</h2>

              <form onSubmit={onSubmit}>

                <div className="form-group form-outline mb-4">
                  <input type="text" name="fullName" id="fullName" className="form-control form-control-lg" value={fullName} placeholder="Full Name" required onChange={onChange}/>
                </div>

                <div className="form-group form-outline mb-4">
                  <input type="email" name="email" id="email" className="form-control form-control-lg" value={email} placeholder="Email" required onChange={onChange}/>
                </div>


                <div className="form-group form-outline mb-4">
                  <input type="text" name="mobileNo" id="mobileNo" className="form-control form-control-lg" value={mobileNo} placeholder="Mobile Number" required onChange={onChange}/>
                </div>

                <div className="form-group form-outline mb-4">
                  <input type="password" name="password" id="password" className="form-control form-control-lg" value={password} placeholder="Password" required onChange={onChange}/>
                  
                </div>

                <div className="form-group form-outline mb-4">
                  <input type="password" name="confirmPassword" id="confirmPassword" className="form-control form-control-lg" value={confirmPassword} placeholder="Confirm Password" required onChange={onChange}/>
                </div>

 

                <div className="form-group d-flex justify-content-center">
                  <button type="submit" className="register-button">Register</button>
                </div>

                <p className="text-center text-muted mt-5 mb-0">Have already an account? <Link to="/login"><u>Login here</u></Link></p>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
		</>

		)
}