import { Container } from 'react-bootstrap'
import { useEffect } from 'react'
import { useDispatch,useSelector } from 'react-redux'
import { getUserOrders,resetOrder } from "../redux/orderSlice"
import { useNavigate } from "react-router-dom"
import Swal from 'sweetalert2'
import OrdersCard from '../components/OrdersCard'

export default function Orders(){


const orders = useSelector(state => state.order.order)
const isError = useSelector(state => state.product.isError)
const isSuccess = useSelector(state => state.product.isSuccess)
const isLoading = useSelector(state => state.product.isLoading)
const message = useSelector(state => state.product.message)

const dispatch = useDispatch();


useEffect(() => {
	if(isError){
		console.log(message)
	}

	
	dispatch(getUserOrders())


	// return () => {
	// 	dispatch(resetOrder())
	// }
},[isError,message, dispatch])



// console.log(orders)


// **************************WHEELS******************************//
		const ordersList = orders.map(order => {
		return (

			<OrdersCard key = {order._id} orderProp = {order}/>
			)
	})

		// console.log(ordersList)






	
	return(
		<>
			<Container className="container">

				<div className="d-flex justify-content-center mt-2 main-font">
				<h3>Order History</h3>
				</div>	
				<div className="mt-3 d-flex justify-content-between main-font">
					<div className="order-product main-font"><h5>Product</h5></div>
					<div><h5>Price</h5></div>
					<div><h5>Quantity</h5></div>
					<div><h5>Total</h5></div>
				</div>
				<hr/>

		{
			orders.map(order => (
			
				<div key={order._id}>

				{order.products.map(product => (
				<div key={product.productId}>

					<div className="d-flex justify-content-between">
            
                		<div className="flex-column align-self-center order-product-name main-font">
                    		<h6>{product.name}</h6>
          				</div>
              			<div className="d-flex" sm={12} md={3}>
                		<div className="flex-column align-self-center main-font">
                   	 		<span>&#8369;</span>{product.price}
                		</div>
              			</div>
              			<div className="d-flex" sm={12} md={3}>
                  		<div className="flex-column align-self-center main-font">
                    		{product.quantity}
                  		</div>
              			</div>

             			<div className="d-flex" sm={12} md={3}>   
                  		<div className="flex-column align-self-center main-font">   
                      		<span>&#8369;</span>{product.quantity*product.price}
                  		</div>     
              		</div>
        
      			</div>

				</div>	
						))}

				      <div>   				          
				            <div className="d-flex justify-content-between main-font">
				                <div><h5 className="text-danger">Order Total</h5></div>
				                <div><h5 className="text-danger">
				                <span>&#8369;</span>{order.totalAmount}</h5></div>
				            </div>
				            <div>   
				                Date Purchased: {order.purchasedOn}
				            </div>
				      </div>
				      <hr/>
				</div>

				))
		}
			

	



		



	

			
				 
			</Container>
		</>

		)

}