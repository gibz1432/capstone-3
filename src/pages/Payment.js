import StripeCheckout from 'react-stripe-checkout'
import { useState, useEffect} from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { createOrder } from "../redux/orderSlice"
import { useSelector, useDispatch } from "react-redux"
import { clearCart } from "../redux/cartRedux"


const KEY = "pk_test_51Kc3YrFiLmYD2QYXQn7s5pJpw7zpA2aEl4Q8tkorcJ2noObXoJnJGxOZOUTURDYWX690rbCiy4WiOmvkfAlgYu4O00rbZHwvfY"

export default function Payment(){

	const [ stripeToken, setStripeToken] = useState(null)
	const navigate = useNavigate();
	const cart = useSelector(state => state.cart);
	const dispatch = useDispatch();

	const onToken = (token) =>{
		setStripeToken(token);
	};


	// *******************CHECKOUT*************************//
	const handleCheckout = () => {
			

	dispatch(createOrder(cart.cartItems))
		
}

	useEffect(() => {
		const makeRequest = async () =>{
			try{
				const res = await axios.post("https://bikada-bikeshop.onrender.com/checkout/payment",{
					tokenId: stripeToken.id, amount: 200000,
				})
				console.log(res.data)
				dispatch(clearCart());
				navigate("/orders")
			}catch(err){
				console.log(err)
			}
		}
		stripeToken && makeRequest()
	},[stripeToken,navigate])

	return(
		<>
			<StripeCheckout name="BiKada Bicycle Shop" image="https://res.cloudinary.com/dsjqz3qsq/image/upload/v1646660124/BikeShop%20Products/logo_go1efe.png" billingAddress
			 	shippingAddress
			 	description = "Your total is Php2000"amount={2000} token={onToken}
			 	stripeKey={KEY}
			 	>

				<button onClick={handleCheckout}>Check out</button>
			</StripeCheckout>	
		</>
		)
}