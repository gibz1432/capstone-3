import { Container, Row, Col, DropdownButton, Dropdown,Button } from 'react-bootstrap'
import { Link,useLocation } from 'react-router-dom'
import {useState, useEffect} from 'react'
import axios from 'axios'
import {addToCart, createCart} from "../redux/cartRedux";
// import { createCart, resetCart } from "../redux/cartSlice"
import {useDispatch,useSelector} from "react-redux";
import { toast } from 'react-toastify'




export default function ProductView(){

const token= localStorage.getItem('token')
	// console.log(token)

const cart = useSelector(state => state.cart);
const isError = useSelector(state => state.cart.isError)
const message = useSelector(state => state.cart.message)
const products = useSelector(state => state.cart.cartItems)

const location = useLocation();
const id = location.pathname.split("/")[2];
const [product, setProduct] = useState({});
// const [quantity, setQuantity] = useState(1);
const cartQuantity = useSelector(state => state.cart.cartQuantity)
const userDetail= JSON.parse(localStorage?.getItem('userDetail'))

 

const [numberOfStocks=product.numberOfStocks, setNumberOfStocks] = useState(product.numberOfStocks)

const productId = id;



const dispatch = useDispatch();

useEffect(() => {

	const getProduct = async() =>{
		try{
			const res = await axios.get("https://bikada-bikeshop.onrender.com/products/" + id)
				setProduct(res.data);


		}catch (err){
			console.log(err)
		}
		
	}
	getProduct();


},[id])

	
	

	const handleCart = () => {
		

		if(numberOfStocks > 0){

			
			setNumberOfStocks(numberOfStocks - 1)
			dispatch(addToCart({...product}))

			

			const productData = {
				productId
			}

			dispatch(createCart(productData))

			const cartData = {
				userId: userDetail._id,
				quantity: cartQuantity
			}

				// if(isError) return alert(message)

		
		}else {
			return alert(`All ${product.name} are in your cart.`)
		}
		
}



	return(
		<>
			<Container className="productView-container">
				<Row>
					<Col sm={12} md={12} lg={4}>
						<img src={product?.img} alt="bike1" className="productView-image mt-5"/>
					</Col>
					<Col sm={12} md={12} lg={8} className='productView-name'>
						<div className="mt-5">
							<h3>{product?.name}</h3>
						</div>						
						<div className="productView-price mt-2">
							 <b><span className="main-font">&#8369;</span></b><b><span className="main-font">{product?.price}</span></b>
						</div>
						<div className="productView-description mt-2 main-font">Description:</div>
						<div className="productView-description-text main-font">
							{product?.description}
						</div>
						<div className="mt-3 main-font">
							Size
						</div>

						<select name="size"  className="productView-menu">
     					{ product?.size?.map((s) => (
							<option key={s} value='large' className="productView-list">{s}</option>
							))
						}
					
  						</select>

  					
  						<div className="mt-3 main-font">
							Color
						</div>
  						<select name="color"  className="productView-menu">
							{ product?.color?.map((c) => (
								<option key={c} value='white' className="productView-list">{c}</option>
								))
							}
  						</select>
  						

  						{ !token ?
  								(

  						<Link to="/login">	
  							<div>
  							<button className="mt-3 button-9">
	  						<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-cart3 mx-1 mb-1" viewBox="0 0 16 16">
	  						<path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
							</svg>
								ADD TO CART</button>
	  						</div>
	  					</Link>
  								)

  								:

  								(
  							<div>
  							<button className="mt-3 button-9" onClick={handleCart}>
	  						<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-cart3 mx-1 mb-1" viewBox="0 0 16 16">
	  						<path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
							</svg>

	  						 ADD TO CART</button>
	  						</div>
  								)
  							}

	  					{/*	<div>
	  						<button className="mt-3 mb-3 button-9">BUY IT NOW</button>
	  						</div>*/}
					</Col>
				</Row>
			</Container>
		</>
		)
}
