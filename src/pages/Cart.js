import { useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"
import { Link } from "react-router-dom"
import { removeFromCart, decreaseCart, addToCart, clearCart, getTotals, createCart, incrementCart, decrementCart, deleteCart, removeCartItem } from "../redux/cartRedux"
// import { createOrder } from "../redux/orderSlice"
import Payment from "./Payment"


export default function Cart(){



	const products = useSelector(state => state.cart.cartItems)
	const isError = useSelector(state => state.cart.isError)
	const message = useSelector(state => state.cart.message)
	// console.log(products)
	const cart = useSelector(state => state.cart);
	const dispatch = useDispatch();

	 useEffect(() => {

    dispatch(getTotals());
  }, [cart, dispatch]);


	// const [numberOfStocks=product.numberOfStocks, setNumberOfStocks] = useState(product.numberOfStocks)
	const cartQuantity = useSelector(state => state.cart.cartQuantity)
	const userDetail= JSON.parse(localStorage?.getItem('userDetail'))
	const cartDb = useSelector(state => state.cartDb)
	


// *******************REMOVE FROM CART*************************//
	const handleRemoveFromCart = (cartItem) =>{
	dispatch(removeFromCart(cartItem));

		let productId = cartItem._id
		console.log(cartItem)

	dispatch(removeCartItem({productId}));

	}


// *******************DECREMENT CART*************************//
	const handleDecreaseCart = (cartItem) =>{
	dispatch(decreaseCart(cartItem));

		const cartData = {
		userId: userDetail._id,
		quantity: cartQuantity
		}
	

		let productId = cartItem._id

		dispatch(decrementCart({productId}));
	}


// *******************INCREMENT CART*************************//
	// const [itemQuantity, setItemQuantity] = 
	const handleIncreaseCart = (cartItem) =>{

		dispatch(addToCart(cartItem));

		let productId = cartItem._id

		dispatch(incrementCart({productId}));

	}


// *******************CLEAR CART*************************//
	const handleClearCart = () =>{
	
	dispatch(clearCart());
	dispatch(deleteCart());
	
	}

// *******************CHECKOUT*************************//
// 	const handleCheckout = () => {
			

// 	dispatch(createOrder(cart.cartItems))
		
// }


	return(
		<div className="cart-container">
				<h2>Shopping Cart</h2>
				{ cart.cartItems?.length === 0? (
					<div className="cart-empty">
						<p>Your cart is currently empty</p>	
					<div className="start-shopping">
						<Link to="/">
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
  						<path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
						</svg>
							<span>Start Shopping</span>
						</Link>
						
					</div>

					</div>

					): (
					<div>
						<div className="titles">
							<h3 className="product-title">Product</h3>
							<h3 className="price">Price</h3>
							<h3 className="quantity">Quantity</h3>
							<h3 className="total">Total</h3>
						</div>
						<div className="cart-items">
							{cart.cartItems?.map(cartItem => (
								<div className="cart-item" key={cartItem._id}>
									<div className="cart-product">
										<img src={cartItem.img}
										alt={cartItem.name}
										/>
										<div>
											<h5>
												{cartItem.name}
												<button className="px-2 cart-remove" onClick={() => handleRemoveFromCart(cartItem)}>
												<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
  												<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
  												<path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
												</svg>
												Remove</button>
											</h5>
										</div>
									</div>
									<div className="cart-product-price">
										<span>&#8369;</span>{cartItem.price}
									</div>
									<div className="cart-product-quantity">
										<button onClick={() => handleDecreaseCart(cartItem)}>-</button>
										<div className="count">
											{cartItem.quantity}
										</div>
										<button onClick={() => handleIncreaseCart(cartItem)}>+</button>
									</div>
									<div className="cart-product-total-price">
										<span>&#8369;</span>{cartItem.price * cartItem.quantity}
									</div>
								</div>
								)
								)
							}
						</div>
						
						<div className="cart-summary">
							<button className="clear-cart mb-4 mx-1" onClick={() => handleClearCart(cart)}>
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-cart-x mx-1" viewBox="0 0 16 16">
  							<path d="M7.354 5.646a.5.5 0 1 0-.708.708L7.793 7.5 6.646 8.646a.5.5 0 1 0 .708.708L8.5 8.207l1.146 1.147a.5.5 0 0 0 .708-.708L9.207 7.5l1.147-1.146a.5.5 0 0 0-.708-.708L8.5 6.793 7.354 5.646z"/>
  							<path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
							</svg>
							Clear Cart</button>
							<div className="cart-checkout">
								<div className="subtotal">
									<span>Subtotal</span>
									<span className="amount">
									<span>&#8369;</span>{cart.cartTotalAmount}
									{console.log(cart)}
									</span>
								</div>
								<p>Taxes and shipping calculated at checkout</p>
								{/*<button onClick={handleCheckout}>Check out</button>*/}
								<Payment/>

								<div className="continue-shopping">
								<Link to="/">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
  								<path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
								</svg>
								<span>Continue Shopping</span>
								</Link>
						
								</div>
							</div>
							
						</div>
			
							
					</div>
					)}	

		</div>

		)
}