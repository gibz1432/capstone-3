import { Navigate, Link } from 'react-router-dom'
import { useSelector} from'react-redux'
import {Container} from 'react-bootstrap'


export default function AdminDashBoard(){

const userDetail = useSelector(state => state.auth.userDetail)



	return(


			(userDetail && !userDetail?.isAdmin) ?

			<Navigate to = "/"/>
			:

		<>
		<Container>
			<div className="d-flex justify-content-center mt-4 mb-3 main-font">
			<h1>Admin Dashboard</h1>
			</div>
			<div className="d-flex justify-content-center">
				<div>
				<Link to="/admin/users" className="dashboard-link">
					<div className="card text-white bg-danger mb-3">
		  			<div className="card-header text-center dashboard-link"><h3>Users</h3></div>
		  			<div className="card-body">
		    		<h5 className="card-title text-center dashboard-link">Show list of users.</h5>
		    		<div className="d-flex justify-content-center">
		    		<svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" fill="currentColor" className="bi bi-people-fill" viewBox="0 0 16 16">
  					<path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
  					<path fillRule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
  					<path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
					</svg>
					</div>
		  			</div>
		  			</div>
		  		</Link>
		  		</div>	

		  		<div className="mx-3">
		  		<Link to="/admin/products" className="dashboard-link">
					<div className="card text-white bg-success mb-3">
		  			<div className="card-header text-center"><h3>Products</h3></div>
		  			<div className="card-body">
		    		<h5 className="card-title text-center">Show list of products.</h5>
		    		<div className="d-flex justify-content-center">
		    		<svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" fill="currentColor" className="bi bi-bicycle" viewBox="0 0 16 16">
  					<path d="M4 4.5a.5.5 0 0 1 .5-.5H6a.5.5 0 0 1 0 1v.5h4.14l.386-1.158A.5.5 0 0 1 11 4h1a.5.5 0 0 1 0 1h-.64l-.311.935.807 1.29a3 3 0 1 1-.848.53l-.508-.812-2.076 3.322A.5.5 0 0 1 8 10.5H5.959a3 3 0 1 1-1.815-3.274L5 5.856V5h-.5a.5.5 0 0 1-.5-.5zm1.5 2.443-.508.814c.5.444.85 1.054.967 1.743h1.139L5.5 6.943zM8 9.057 9.598 6.5H6.402L8 9.057zM4.937 9.5a1.997 1.997 0 0 0-.487-.877l-.548.877h1.035zM3.603 8.092A2 2 0 1 0 4.937 10.5H3a.5.5 0 0 1-.424-.765l1.027-1.643zm7.947.53a2 2 0 1 0 .848-.53l1.026 1.643a.5.5 0 1 1-.848.53L11.55 8.623z"/>
					</svg>
					</div>
		  			</div>
		  			</div>
		  		</Link>	
		  		</div>	


		  		<div>
		  		<Link to="/admin/orders" className="dashboard-link">
					<div className="card text-white bg-primary mb-3">
		  			<div className="card-header text-center"><h3>Orders</h3></div>
		  			<div className="card-body">
		    		<h5 className="card-title text-center">Show list of orders.</h5>
		    		<div className="d-flex justify-content-center">
		    	<svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" fill="currentColor" className="bi bi-list-check" viewBox="0 0 16 16">
  				<path fillRule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
				</svg>
					</div>
		  			</div>
		  			</div>
		  		</Link>	
		  		</div>	
	  		</div>	
  		</Container>
  			


		</>

		)



}