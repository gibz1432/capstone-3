import { Link } from 'react-router-dom'

export default function PageNotFound(){
	return(
		<>	<div className="d-flex justify-content-center mt-5 main-font">
				<div className="d-flex flex-column">
					<div><h3 className="text-center">404</h3></div>
					<div><h3>Page Not Found</h3></div>
					<div className="start-shopping d-flex justify-content-center">
						<Link to="/">
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
  						<path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
						</svg>
							<span>Go back to homepage</span>
						</Link>
						
					</div>
				</div>	
			</div>	
		</>


		)
}