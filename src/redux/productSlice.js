import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import productService from './productService'

const initialState = {
	products: [],
	bikes: [],
	frames: [],
	helmets: [],
	tires: [],
	wheels: [],
	isError: false,
	isSuccess: false,
	isLoading: false,
	message: ''
}

//Create new product
export const createProduct = createAsyncThunk('product/create', async (productData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await productService.createProduct(productData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})

//****************Get Bikes***********************//

export const getBikes = createAsyncThunk('product/getBikes', async(_,thunkAPI) => {
	try{
		
		return await productService.getBikes()
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}

})

//****************Get Frames ***********************//
export const getFrames = createAsyncThunk('product/getFrames', async(_,thunkAPI) => {
	try{
		
		return await productService.getFrames()
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}

})


//****************Get Helmets ***********************//
export const getHelmets = createAsyncThunk('product/getHelmets', async(_,thunkAPI) => {
	try{
		
		return await productService.getHelmets()
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//****************Get Tires ***********************//
export const getTires = createAsyncThunk('product/getTires', async(_,thunkAPI) => {
	try{
		
		return await productService.getTires()
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//****************Get Wheels ***********************//
export const getWheels = createAsyncThunk('product/getWheels', async(_,thunkAPI) => {
	try{
		
		return await productService.getWheels()
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})

//****************Get all Products***********************//

export const getAllProducts = createAsyncThunk('product/getAllProducts', async(_,thunkAPI) => {
  try{
    const token = thunkAPI.getState().auth.token
    return await productService.getAllProducts(token)
  }catch(error){
    const message = error.response.data || error.message || error.toString()
    return thunkAPI.rejectWithValue(message)
  }

})


//*****************************update Product***********************************//
export const updateProduct = createAsyncThunk('product/updateProduct', async (productId,productData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await productService.updateProduct(productId,productData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//********Disable a product**************//
export const disableProduct = createAsyncThunk('product/disableProduct', async (productData, thunkAPI) =>{
  try{
    const token = thunkAPI.getState().auth.token
    return await productService.disableProduct(productData, token)
  }catch(error){
    const message = error.response.data || error.message || error.toString()
    return thunkAPI.rejectWithValue(message)
  }
})





export const productSlice = createSlice({
	name: 'product',
	initialState,
	reducers: {
		reset: (state) => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(createProduct.pending, (state) => {
				state.isLoading = true
			})
			.addCase(createProduct.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.products.push(action.payload)
			})
			.addCase(createProduct.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************BIKES*****************//
			.addCase(getBikes.pending, (state) => {
				state.isLoading = true
			})
			.addCase(getBikes.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.bikes = action.payload
			})
			.addCase(getBikes.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************FRAMES*****************//
			.addCase(getFrames.pending, (state) => {
			state.isLoading = true
			})
			.addCase(getFrames.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.frames = action.payload
			})
			.addCase(getFrames.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************HELMETS*****************//
			.addCase(getHelmets.pending, (state) => {
			state.isLoading = true
			})
			.addCase(getHelmets.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.helmets = action.payload
			})
			.addCase(getHelmets.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************TIRES*****************//
			.addCase(getTires.pending, (state) => {
			state.isLoading = true
			})
			.addCase(getTires.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.tires = action.payload
			})
			.addCase(getTires.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************WHEELS*****************//
			.addCase(getWheels.pending, (state) => {
			state.isLoading = true
			})
			.addCase(getWheels.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.wheels = action.payload
			})
			.addCase(getWheels.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			      // ************Get All Products*****************//
		    .addCase(getAllProducts.pending, (state) => {
		     	state.isLoading = true
		      })
		    .addCase(getAllProducts.fulfilled, (state,action) => {
		        state.isLoading = false
		        state.products = action.payload
		      })
		    .addCase(getAllProducts.rejected, (state,action) => {
		        state.isLoading = false
		        state.isError = true
		        state.message = action.payload
		      })

				    // ************update product*****************//
					.addCase(updateProduct.pending, (state) => {
					state.isLoading = true
					})
					.addCase(updateProduct.fulfilled, (state,action) => {
						state.isLoading = false
						state.isSuccess = true
						state.products = action.payload
					})
					.addCase(updateProduct.rejected, (state,action) => {
						state.isLoading = false
						state.isError = true
						state.message = action.payload
					})

					// ************disable product*****************//
					.addCase(disableProduct.pending, (state) => {
					state.isLoading = true
					})
					.addCase(disableProduct.fulfilled, (state,action) => {
						state.isLoading = false
						state.isSuccess = true
						state.products = action.payload
					})
					.addCase(disableProduct.rejected, (state,action) => {
						state.isLoading = false
						state.isError = true
						state.message = action.payload
					})


	}
})

export const {reset} = productSlice.actions
export default productSlice.reducer