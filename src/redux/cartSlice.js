import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import cartService from './cartService'

const initialState = {
	cart:{} ,
	cartQuantity: 0,
	isError: false,
	isSuccess: false,
	isLoading: false,
	message: ''
}

//Add to cart
export const createCart = createAsyncThunk('cart/createCart', async (productData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.createCart(productData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})

//****************Get Cart***********************//

export const getCart = createAsyncThunk('cart/getCart', async(token,thunkAPI) => {
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.getCart(token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}

})




export const cartSlice = createSlice({
	name: 'cart',
	initialState,
	reducers: {
		resetCart: (state) => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(createCart.pending, (state) => {
				state.isLoading = true
			})
			.addCase(createCart.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.cart = action.payload
				state.cartQuantity += 1


			})
			.addCase(createCart.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
				
			})
			// ***************getCart****************//
			.addCase(getCart.pending, (state) => {
				state.isLoading = true
			})
			.addCase(getCart.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.cart = action.payload
				state.cartQuantity += 1
				
				// console.log(state.cart)

			})
			.addCase(getCart.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})

	}
})

export const {resetCart} = cartSlice.actions
export default cartSlice.reducer