import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import cartService from './cartService'
import { toast } from "react-toastify";




const initialState = {
	cartItems: [],
	cartDb: null,
	cartTotalQuantity:0,
	cartTotalAmount: 0,
	cartQuantity:0,
	isError: false,
	message: ''
}

//Add to cart
export const createCart = createAsyncThunk('cartItems/createCart', async (productData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.createCart(productData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//****************Get Cart***********************//

export const getCart = createAsyncThunk('cartItems/getCart', async(_,thunkAPI) => {
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.getCart(token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}

})


//Increment cart
export const incrementCart = createAsyncThunk('cartItems/incrementCart', async (cartData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.incrementCart(cartData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//Decrement cart
export const decrementCart = createAsyncThunk('cartItems/decrementCart', async (cartData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.decrementCart(cartData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//Delete cart
export const deleteCart = createAsyncThunk('cartItems/deleteCart', async (_, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.deleteCart(token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//Remove cart
export const removeCartItem = createAsyncThunk('cartItems/removeCartItem', async (cartData, thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await cartService.removeCartItem(cartData, token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})






const cartSlice = createSlice({
	name: "cart",
	initialState,
	reducers:{


		addToCart:(state,action) => {
			const itemIndex = state.cartItems.findIndex(
			(item) => item._id === action.payload._id
			);

			if (action.payload.numberOfStocks == action.payload.quantity)
				return alert('All items are in your cart')


				// state.cartItems[itemIndex].quantity) return alert('All items are in your cart.')

			if(itemIndex >= 0){
				state.cartItems[itemIndex].quantity += 1
				state.cartQuantity +=1
				

				toast.info(`increased ${state.cartItems[itemIndex].name}cart quantity`, {
					position: "bottom-right"
				});
			} else{
	
			const tempProduct = {...action.payload, quantity: 1 };
			state.cartItems.push(tempProduct);
			state.cartQuantity +=1
			
				toast.success(`${action.payload.name} successfully added to cart`, {
					position: "bottom-right"
				});
			}
			
		},

		removeFromCart:(state,action) => {
			const nextCartItems = state.cartItems.filter(
				cartItem => cartItem._id !== action.payload._id
				)
			state.cartItems = nextCartItems;
			state.cartQuantity -= action.payload.quantity

			toast.success(`${action.payload.name} successfully removed from Cart`, {
					position: "bottom-right"
			});
		},

		decreaseCart:(state,action) => {


			const itemIndex = state.cartItems.findIndex(
					cartItem => cartItem._id === action.payload._id
				)
			if(state.cartItems[itemIndex].quantity > 1){
				state.cartItems[itemIndex].quantity -= 1
				state.cartQuantity -= 1

			toast.info(`Decreased ${action.payload.name} Cart quantity`, {
					position: "bottom-right"
			});

			} else if(state.cartItems[itemIndex].quantity === 1){
				const nextCartItems = state.cartItems.filter(
				cartItem => cartItem._id !== action.payload._id
				)
				state.cartItems = nextCartItems;
				state.cartQuantity -= 1

				toast.success(`${action.payload.name} successfully removed from Cart`, {
					position: "bottom-right"
			});
			}

		},

		clearCart:(state, action) => {
			state.cartItems = []
			state.cartQuantity = 0
			toast.success(`Cart cleared`, {
			position: "bottom-right"
			});
		},

		getTotals:(state,action) => {

		let {total, quantity} = state.cartItems.reduce((cartTotal, cartItem) =>{
			const { price, quantity } = cartItem;
			const itemTotal = price *quantity;
			cartTotal.total += itemTotal
			cartTotal.quantity += quantity

			return cartTotal;
			},
			{	
				total:0,
				quantity: 0
			}
		);

		state.cartTotalQuantity = quantity;
		state.cartTotalAmount = total;
		
		}

	},
		extraReducers: (builder) => {
		builder
			.addCase(createCart.rejected, (state,action) => {
			state.isError = true
			state.message = action.payload
			})

			.addCase(getCart.fulfilled, (state,action) => {
			state.isError = false
			})
			// ***********increment cart***********************//
			.addCase(incrementCart.rejected, (state,action) => {
			state.isError = true
			state.message = action.payload
			})

			.addCase(incrementCart.fulfilled, (state,action) => {
			state.isError = false
			})
			// ***********decrement cart***********************//
			.addCase(decrementCart.rejected, (state,action) => {
			state.isError = true
			state.message = action.payload
			})

			.addCase(decrementCart.fulfilled, (state,action) => {
			state.isError = false
			})

			// ***********delete cart***********************//
			.addCase(deleteCart.rejected, (state,action) => {
			state.isError = true
			state.message = action.payload
			})

			.addCase(deleteCart.fulfilled, (state,action) => {
			state.isError = false
			})

			// ***********remove cartItem***********************//
			.addCase(removeCartItem.rejected, (state,action) => {
			state.isError = true
			state.message = action.payload
			})

			.addCase(removeCartItem.fulfilled, (state,action) => {
			state.isError = false
			})


	}
});



export const {addToCart, removeFromCart, decreaseCart, clearCart, getTotals} = cartSlice.actions
export default cartSlice.reducer;