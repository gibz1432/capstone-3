import axios from 'axios'

//add to cart
const createCart = async(productData, token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}


	const response = await axios.post('https://bikada-bikeshop.onrender.com/carts', productData, config)


	return response.data
}



// get cart
const getCart = async(token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.get('https://bikada-bikeshop.onrender.com/carts', config)

	

	return response.data
}


//increment cart
const incrementCart = async(cartData, token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}


	const response = await axios.put('https://bikada-bikeshop.onrender.com/carts/increment', cartData, config)

	
	return response.data
}


//decrement cart
const decrementCart = async(cartData, token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.put('https://bikada-bikeshop.onrender.com/carts/decrement', cartData, config)

	
	return response.data
}


//delete cart
const deleteCart = async(token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.delete('https://bikada-bikeshop.onrender.com/carts/delete', config)

	
	return response.data
}

//remove cartItem
const removeCartItem = async(cartData, token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.put('https://bikada-bikeshop.onrender.com/carts/removeCartItem', cartData, config)

	
	return response.data
}




const cartService = {
	createCart,
	getCart,
	incrementCart,
	decrementCart,
	deleteCart,
	removeCartItem,
	
}

export default cartService

