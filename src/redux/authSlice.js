import { createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import authService from './authService'

//Get user from localStorage
const token = JSON.parse(localStorage.getItem('token'))


const initialState = {
	token: token? token: null,
	user: [],
	userDetail: null,
  isAdmin: "false",
	isError: false,
	isSuccess: false,
	isLoading: false,
	message: ''
}

//Register user
export const register = createAsyncThunk('auth/register', async (user, thunkAPI) => {
	try{
		return await authService.register(user)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})

// Login user
export const login = createAsyncThunk('auth/login', async (token, thunkAPI) => {
  try {
    return await authService.login(token)
  } catch (error) {
    const message =
      error.response.data ||
      error.message ||
      error.toString()
    return thunkAPI.rejectWithValue(message)
  }
})

// getUserDetail
export const getUserDetail = createAsyncThunk('auth/getUserDetail', async (token, thunkAPI) => {
  try {
    return await authService.getUserDetail(token)
  } catch (error) {
    const message =
      error.response.data ||
      error.message ||
      error.toString()
    return thunkAPI.rejectWithValue(message)
  }
})


//****************Get all Users***********************//

export const getAllUsers = createAsyncThunk('auth/getAllUsers', async(_,thunkAPI) => {
  try{
    const token = thunkAPI.getState().auth.token
    return await authService.getAllUsers(token)
  }catch(error){
    const message = error.response.data || error.message || error.toString()
    return thunkAPI.rejectWithValue(message)
  }

})

//********Set user as admin**************//
export const setAdmin = createAsyncThunk('auth/setAdmin', async (userData, thunkAPI) =>{
  try{
    const token = thunkAPI.getState().auth.token
    return await authService.setAdmin(userData, token)
  }catch(error){
    const message = error.response.data || error.message || error.toString()
    return thunkAPI.rejectWithValue(message)
  }
})



export const logout = createAsyncThunk('auth/logout', async () => {
  await authService.logout()
})

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    reset: (state) => {
      state.isLoading = false
      state.isSuccess = false
      state.isError = false
      state.message = ''
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(register.pending, (state) => {
        state.isLoading = true
      })
      .addCase(register.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.user = action.payload
      })
      .addCase(register.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
        state.user = null
      })
      .addCase(login.pending, (state) => {
        state.isLoading = true
      })
      .addCase(login.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.token = action.payload
      })
      .addCase(login.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
        state.token = null
      })
      .addCase(logout.fulfilled, (state) => {
        state.token = null
      })
      .addCase(getUserDetail.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getUserDetail.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.userDetail = action.payload
      })
      .addCase(getUserDetail.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
        state.userDetail = null
      })
      // ************Get All Users*****************//
      .addCase(getAllUsers.pending, (state) => {
      state.isLoading = true
      })
      .addCase(getAllUsers.fulfilled, (state,action) => {
        state.isLoading = false
        state.isSuccess = true
        state.user = action.payload
      })
      .addCase(getAllUsers.rejected, (state,action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
      // ************Get setAdmin*****************//
      .addCase(setAdmin.pending, (state) => {
      state.isLoading = true
      })
      .addCase(setAdmin.fulfilled, (state,action) => {
        state.isLoading = false
        state.isSuccess = true
        state.isAdmin = 'true'
        
      })
      .addCase(setAdmin.rejected, (state,action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })


  },
})

export const { reset, setToAdmin } = authSlice.actions
export default authSlice.reducer