import axios from 'axios'

//Create new product
const createProduct = async(productData, token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.post('https://bikada-bikeshop.onrender.com/products', productData, config)

	return response.data
}


//*****************************Get Bikes**********************//
const getBikes = async() => {


	const response = await axios.get('https://bikada-bikeshop.onrender.com/categories/products/630b6cad8e2b36103db128f1', {mode:'cors'})

	return response.data
}

//*****************************Get Frames**********************//
const getFrames = async() => {


	const response = await axios.get('https://bikada-bikeshop.onrender.com/categories/products/630b6f9e1d334dcfce151d12', {mode:'cors'})

	return response.data
}

//*****************************Get Helmets**********************//
const getHelmets = async() => {


	const response = await axios.get('https://bikada-bikeshop.onrender.com/categories/products/630b714d1d334dcfce151d18', {mode:'cors'})

	return response.data
}


//*****************************Get Tires**********************//
const getTires = async() => {


	const response = await axios.get('https://bikada-bikeshop.onrender.com/categories/products/630b71671d334dcfce151d1e', {mode:'cors'})

	return response.data
}

//*****************************Get Wheels**********************//
const getWheels = async() => {


	const response = await axios.get('https://bikada-bikeshop.onrender.com/categories/products/630b71611d334dcfce151d1b', {mode:'cors'})

	return response.data
}

//*****************************Get All Products**********************//
const getAllProducts = async(token) => {

  const config = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }

  const response = await axios.get('https://bikada-bikeshop.onrender.com/products/all',config)

  return response.data
}

//*****************update Product*********************//
const updateProduct = async(productId,productData, token) => {
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.put(`https://bikada-bikeshop.onrender.com/products/${productId}`, productData, config)

	
	return response.data
}



//**************Disable Product******************//
const disableProduct = async(productData, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }


  const response = await axios.post('https://bikada-bikeshop.onrender.com/products/archive', productData, config)

  
  return response.data
}




const productService = {
	createProduct,
	getBikes,
	getFrames,
	getHelmets,
	getTires,
	getWheels,
	getAllProducts,
	updateProduct,
	disableProduct,
}


export default productService