import axios from 'axios'

//add to cart
const createOrder = async(cartData,token) => {

	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.post('https://bikada-bikeshop.onrender.com/carts/checkout', cartData, config)

	
	return response.data

} 



//*****************************Get UserOrders**********************//
const getUserOrders = async(token) => {

	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.get('https://bikada-bikeshop.onrender.com/orders',config)

	return response.data
}


//*****************************Get UserOrders**********************//
const getAllOrders = async(token) => {

	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	const response = await axios.get('https://bikada-bikeshop.onrender.com/orders/all',config)

	return response.data
}






const orderService = {
	createOrder,
	getUserOrders,
	getAllOrders,

}

export default orderService

