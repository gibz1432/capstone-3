import axios from 'axios';
// import Swal from 'sweetalert2'

// const API_URL = 'http://localhost:4000/users/'

//Register user
const register = async (userData) => {
  
      const response = await axios.post('https://bikada-bikeshop.onrender.com/users/register', userData)

      // if (response.data) {
      //   localStorage.setItem('user', JSON.stringify(response.data))
      // }
      return response.data
      
}

// Login user
const login = async (userData) => {

      const response = await axios.post('https://bikada-bikeshop.onrender.com/users/login', userData)

      if (response.data) {
        localStorage.setItem('token', JSON.stringify(response.data))
      }

      return response.data
}

// Get user detail
const getUserDetail = async (token) => {
      
      const response = await fetch('https://bikada-bikeshop.onrender.com/users/details', {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
       const data = await response.json();

        if (data) {
        localStorage.setItem('userDetail', JSON.stringify(data))
      }
      return data
}



// Logout user
const logout = () => {
  localStorage.removeItem('token')
  localStorage.removeItem('userDetail')

}


//*****************************Get All Users**********************//
const getAllUsers = async(token) => {

  const config = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }

  const response = await axios.get('https://bikada-bikeshop.onrender.com/users/all',config)

  return response.data
}


//**************Set Admin******************//
const setAdmin = async(userData, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }


  const response = await axios.post('https://bikada-bikeshop.onrender.com/users/setAdmin', userData, config)

  
  return response.data
}




const authService = {
  register,
  logout,
  login,
  getUserDetail,
  getAllUsers,
  setAdmin,
}

export default authService




