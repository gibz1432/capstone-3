import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import orderService from './orderService'

const initialState = {
	order:[] ,
	isError: false,
	isSuccess: false,
	isLoading: false,
	message: ''
}

//Add to cart
export const createOrder = createAsyncThunk('order/createOrder', async (cartData,thunkAPI) =>{
	try{
		const token = thunkAPI.getState().auth.token
		return await orderService.createOrder(cartData,token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})

//****************Get Individual Order***********************//

export const getUserOrders = createAsyncThunk('order/getUserOrders', async(_,thunkAPI) => {
	try{
		const token = thunkAPI.getState().auth.token
		return await orderService.getUserOrders(token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})


//****************Get All Orders***********************//

export const getAllOrders = createAsyncThunk('order/getAllOrders', async(_,thunkAPI) => {
	try{
		const token = thunkAPI.getState().auth.token
		return await orderService.getAllOrders(token)
	}catch(error){
		const message = error.response.data || error.message || error.toString()
		return thunkAPI.rejectWithValue(message)
	}
})







export const orderSlice = createSlice({
	name: 'order',
	initialState,
	reducers: {
		resetOrder: (state) => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(createOrder.pending, (state) => {
				state.isLoading = true
			})
			.addCase(createOrder.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.order.push(action.payload)
				


			})
			.addCase(createOrder.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************User orders*****************//
			.addCase(getUserOrders.pending, (state) => {
			state.isLoading = true
			})
			.addCase(getUserOrders.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.order = action.payload
			})
			.addCase(getUserOrders.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})
			// ************Get all orders*****************//
			.addCase(getAllOrders.pending, (state) => {
			state.isLoading = true
			})
			.addCase(getAllOrders.fulfilled, (state,action) => {
				state.isLoading = false
				state.isSuccess = true
				state.order = action.payload
			})
			.addCase(getAllOrders.rejected, (state,action) => {
				state.isLoading = false
				state.isError = true
				state.message = action.payload
			})

	}
})

export const {resetOrder} = orderSlice.actions
export default orderSlice.reducer